# CPU_8bit_verilog

Remember to read `Quick Start Guide to Verilog by Brock J.LaMeres` book, If you are a noob like me :).  

## Requirements

Vivado 2022.1

## Description

This repository contain a 8-bit CPU with a basic instruction set and 224 bytes of Ram, the rest of the address are using for peripherial devices. You can find more info about ISA and architecture in chapter 11 of `Quick Start Guide to Verilog by Brock J.LaMeres` book.

## Improvements

- ALU operations.
- Data in Interrupt.
- Device interrupt like a timer.

## Challenges

- More ALU operations like multiply and divided.
- Conditional operations.
- Shift operations. 
- Pipeline FSM.
- Multicore.


## Contributing
This is an public repository of Juan C. Rodríguez and is open for contributions.

