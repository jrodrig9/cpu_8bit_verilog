`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.08.2022 18:24:05
// Design Name: 
// Module Name: tb_cpu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_cpu();

    wire [7:0] port_out_00;
    wire [7:0] port_out_01;
    wire [7:0] port_out_02;
    wire [7:0] port_out_03;
    wire [7:0] port_out_04;
    wire [7:0] port_out_05;
    reg [7:0] port_in_00;
    reg [7:0] port_in_01;
    reg [7:0] port_in_02;
    reg [7:0] port_in_03;
    reg [7:0] port_in_04;
    reg [7:0] port_in_05;
    reg reset;
    reg clock;
    
    
    cpu core0(
        .port_out_00(port_out_00),
        .port_out_01(port_out_01),
        .port_out_02(port_out_02),
        .port_out_03(port_out_03),
        .port_out_04(port_out_04),
        .port_out_05(port_out_05),
        .port_in_00(port_in_00),
        .port_in_01(port_in_01),
        .port_in_02(port_in_02),
        .port_in_03(port_in_03),
        .port_in_04(port_in_04),
        .port_in_05(port_in_05),
        .clock(clock),
        .reset(reset) 
    );
    
    always #5 clock = ~clock;
    //always #2000 port_in_05 = port_in_05 + 1;
    
    initial
    begin
        clock <= 0;
        reset <= 0;
        port_in_00 <= 5;
        port_in_01 <= 0;
        port_in_02 <= 10;
        port_in_03 <= 0;
        port_in_04 <= 0;
        
        #80 port_in_05 <= 0;
        #10 reset = 1;
    end
    
endmodule
