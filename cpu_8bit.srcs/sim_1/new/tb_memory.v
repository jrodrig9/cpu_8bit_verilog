`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 13:19:50
// Design Name: 
// Module Name: tb_memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_memory();

    wire [7:0] data_out;
    wire [7:0] port_out_00;
    wire [7:0] port_out_01;
    wire [7:0] port_out_02;
    wire [7:0] port_out_03;
    wire [7:0] port_out_04;
    wire [7:0] port_out_05;
    reg [7:0] address;
    reg [7:0] data_in;
    reg write;
    reg [7:0] port_in_00;
    reg [7:0] port_in_01;
    reg [7:0] port_in_02;
    reg [7:0] port_in_03;
    reg [7:0] port_in_04;
    reg [7:0] port_in_05;
    reg reset;
    reg clock;
    
    memory test(
        .data_out(data_out),
        .port_out_00(port_out_00),
        .port_out_01(port_out_01),
        .port_out_02(port_out_02),
        .port_out_03(port_out_03),
        .port_out_04(port_out_04),
        .port_out_05(port_out_05),
        .address(address),
        .data_in(data_in),
        .write(write),
        .port_in_00(port_in_00),
        .port_in_01(port_in_01),
        .port_in_02(port_in_02),
        .port_in_03(port_in_03),
        .port_in_04(port_in_04),
        .port_in_05(port_in_05),
        .reset(reset),
        .clock(clock)
        );
    
    always #5 clock = ~clock;
    always #10 address = address + 1;
    
    initial
    begin
        clock <= 0;
        address <= 0;
        reset <= 0;
        data_in <= 0;
        write <= 0;
        port_in_00 <= 0;
        port_in_01 <= 1;
        port_in_02 <= 2;
        port_in_03 <= 3;
        port_in_04 <= 4;
        port_in_05 <= 5;
        
        #10 reset <= 1;
        #70 address <= 230;
        #70 address <= 224;
        #1 write <= 1;
        #2 data_in <= 5;
        #70 $finish;
    end
    
endmodule
