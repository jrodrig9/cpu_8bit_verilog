`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 19:05:42
// Design Name: 
// Module Name: tb_data_path
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_data_path();

    wire [7:0] to_memory;
    wire [7:0] address;
    wire [7:0] ir;
    wire [3:0] ccr;
    wire [7:0] from_memory;
    reg [2:0] alu_sel;
    reg [1:0] bus2_sel;
    reg [1:0] bus1_sel;
    reg ir_load;
    reg mar_load;
    reg pc_load;
    reg pc_inc;
    reg a_load;
    reg b_load;
    reg ccr_load;
    reg reset;
    reg clock;
    
    reg write;
    wire [7:0] port_out_00;
    wire [7:0] port_out_01;
    wire [7:0] port_out_02;
    wire [7:0] port_out_03;
    wire [7:0] port_out_04;
    wire [7:0] port_out_05;
    reg [7:0] port_in_00;
    reg [7:0] port_in_01;
    reg [7:0] port_in_02;
    reg [7:0] port_in_03;
    reg [7:0] port_in_04;
    reg [7:0] port_in_05;
        
    data_path data(
        .to_memory(to_memory),
        .address(address),
        .ir(ir),
        .ccr(ccr),
        .from_memory(from_memory),
        .alu_sel(alu_sel),
        .bus2_sel(bus2_sel),
        .bus1_sel(bus1_sel),
        .ir_load(ir_load),
        .mar_load(mar_load),
        .pc_load(pc_load),
        .pc_inc(pc_inc),
        .a_load(a_load),
        .b_load(b_load),
        .ccr_load(ccr_load),
        .reset(reset),
        .clock(clock)
        );
        
   
   memory test(
        .data_out(from_memory),
        .port_out_00(port_out_00),
        .port_out_01(port_out_01),
        .port_out_02(port_out_02),
        .port_out_03(port_out_03),
        .port_out_04(port_out_04),
        .port_out_05(port_out_05),
        .address(address),
        .data_in(to_memory),
        .write(write),
        .port_in_00(port_in_00),
        .port_in_01(port_in_01),
        .port_in_02(port_in_02),
        .port_in_03(port_in_03),
        .port_in_04(port_in_04),
        .port_in_05(port_in_05),
        .reset(reset),
        .clock(clock)
        );
        
   always #5 clock = ~clock;
   
   initial
   begin
   
      //GET FROM MEMORY 0
      clock <= 0;
      ir_load <= 0;
      mar_load <= 0;
      pc_load <= 0;
      pc_inc <= 0;
      a_load <= 0;
      b_load <= 0;
      ccr_load <= 0;
      write <= 0;
      reset <= 0;
      clock <= 0;
      bus1_sel <= 0;
      bus2_sel <= 2'b10;
      alu_sel <= 0;
      port_in_00 <= 0;
      port_in_01 <= 1;
      port_in_02 <= 2;
      port_in_03 <= 3;
      port_in_04 <= 4;
      port_in_05 <= 5;
      
      //GET FROM MEMORY 1, MOVE PC TO MAR
      #10 reset <= 1;
      #10 pc_inc <= 1;
      #10 pc_inc <= 0;
      #10 bus2_sel <= 2'b01;
      #10 mar_load <=1;
      #10 mar_load <= 0;
      
      //WRITE MAR TO MEMORY ADDRESS 1
      #10 write <= 1;
      
      //READ MEMORY ADDRESS 1
      #10 write <= 0;
      
      //TEST ALU
      #10 bus2_sel <= 2'b10;
      #10 bus1_sel <= 2'b01;
      #10 a_load <= 1;
      #10 a_load <= 0;
      #10 bus2_sel <= 2'b00;
      #10 b_load <= 1;
      #10 b_load <= 0;
      #10 mar_load <= 1;
      #10 mar_load <= 0;
      #10 ccr_load <= 1;
      #10 ccr_load <= 0;

   end

endmodule
