`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 12:02:41
// Design Name: 
// Module Name: rw_224x8_async
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rw_200x8_async(
    output reg [7:0] data_out,
    input wire [7:0] address,
    input wire [7:0] data_in,
    input wire write,
    input wire clock
    );
    
    reg enable;
    reg [7:0] rw_mem [0:199];
    
    initial
    begin
        enable <= 0;
        //INIT
        
        //CONFIG TIMER
        rw_mem[0] <= 8'h86;
        rw_mem[1] <= 8'h01;
        
        rw_mem[2] <= 8'h96;
        rw_mem[3] <= 8'hC8;
        
        rw_mem[4] <= 8'h86;
        rw_mem[5] <= 8'hFF;
        
        rw_mem[6] <= 8'h96;
        rw_mem[7] <= 8'hC9;
        
        //CONFIG IRQ
        rw_mem[8] <= 8'h98;
        rw_mem[9] <= 8'h13;
        
        //INIT DATA
        rw_mem[10] <= 8'h87;
        rw_mem[11] <= 8'hE6;
        
        rw_mem[12] <= 8'h89;
        rw_mem[13] <= 8'hE7;
        
        //WHILE(1)
        rw_mem[14] <= 8'h46;
        rw_mem[15] <= 8'h96;
        rw_mem[16] <= 8'hE0;

        rw_mem[17] <= 8'h20;
        rw_mem[18] <= 8'h0E;
        
        //IRQ
        /*rw_mem[19] <= 8'h89;
        rw_mem[20] <= 8'hEB;

        rw_mem[21] <= 8'h47;
        
        rw_mem[22] <= 8'h97;
        rw_mem[23] <= 8'hE1;*/
        
        rw_mem[19] <= 8'h47;
        rw_mem[20] <= 8'h99;
    end
    
    always @ (address)
    begin
        if (address >= 0 && address < 200)
            enable <= 1;
        else
            enable <= 0;
    end
    
    always @ (posedge clock)
    begin
        if (write && enable)
            rw_mem[address] = data_in;
            
         else if (!write && enable)
            data_out = rw_mem[address];
            
    end
    
    
endmodule
