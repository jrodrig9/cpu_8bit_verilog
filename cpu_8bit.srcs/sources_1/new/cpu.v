`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.08.2022 17:43:14
// Design Name: 
// Module Name: cpu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cpu(
    output wire [7:0] port_out_00,
    output wire [7:0] port_out_01,
    output wire [7:0] port_out_02,
    output wire [7:0] port_out_03,
    output wire [7:0] port_out_04,
    output wire [7:0] port_out_05,
    input wire [7:0] port_in_00,
    input wire [7:0] port_in_01,
    input wire [7:0] port_in_02,
    input wire [7:0] port_in_03,
    input wire [7:0] port_in_04,
    input wire [7:0] port_in_05,
    input wire clock,
    input wire reset 
    );
    
    wire [7:0] to_memory;
    wire [7:0] address;
    wire [7:0] ir;
    wire [3:0] ccr_aux;
    wire [7:0] from_memory;
    
    wire [2:0] alu_sel;
    wire [1:0] bus2_sel;
    wire [1:0] bus1_sel;
    wire ir_load;
    wire mar_load;
    wire pc_load;
    wire pc_inc;
    wire a_load;
    wire b_load;
    wire ccr_load;
    wire irq_load;    
    wire backup_load;
    wire backup_restore;
    wire [7:0] irq_number;
    wire irq_number_load;
    wire irq_pending;
    wire reset_irq_pending;
    wire write;

    control_unit logic( 
        .ir_load(ir_load),
        .mar_load(mar_load),
        .pc_load(pc_load),
        .pc_inc(pc_inc),
        .a_load(a_load),
        .b_load(b_load),
        .alu_sel(alu_sel),
        .ccr_load(ccr_load),
        .irq_load(irq_load),
        .reset_irq_pending(reset_irq_pending),
        .bus2_sel(bus2_sel),
        .bus1_sel(bus1_sel),
        .backup_load(backup_load),
        .backup_restore(backup_restore),
        .write(write),
        .ir(ir),
        .irq_pending(irq_pending),
        .ccr_result(ccr_aux),
        .reset(reset),
        .clock(clock)
    );
    
    data_path registers(
        .to_memory(to_memory),
        .address(address),
        .ir(ir),
        .ccr(ccr_aux),
        .from_memory(from_memory),
        .alu_sel(alu_sel),
        .bus2_sel(bus2_sel),
        .bus1_sel(bus1_sel),
        .irq_number_in(irq_number),
        .ir_load(ir_load),
        .mar_load(mar_load),
        .pc_load(pc_load),
        .pc_inc(pc_inc),
        .a_load(a_load),
        .b_load(b_load),
        .ccr_load(ccr_load),
        .irq_load(irq_load),
        .backup_load(backup_load),
        .backup_restore(backup_restore),
        .irq_number_load(irq_number_load),
        .reset(reset),
        .clock(clock)
        );
        
   
   memory devices(
        .data_out(from_memory),
        .port_out_00(port_out_00),
        .port_out_01(port_out_01),
        .port_out_02(port_out_02),
        .port_out_03(port_out_03),
        .port_out_04(port_out_04),
        .port_out_05(port_out_05),
        .irq_number(irq_number),
        .irq_number_load(irq_number_load),
        .irq_pending(irq_pending),
        .address(address),
        .data_in(to_memory),
        .write(write),
        .reset_irq_pending(reset_irq_pending),
        .port_in_00(port_in_00),
        .port_in_01(port_in_01),
        .port_in_02(port_in_02),
        .port_in_03(port_in_03),
        .port_in_04(port_in_04),
        .port_in_05(port_in_05),
        .reset(reset),
        .clock(clock)
        );
       
endmodule
