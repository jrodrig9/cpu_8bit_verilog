`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 14:18:46
// Design Name: 
// Module Name: data_path
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module control_unit( 
    output reg ir_load,
    output reg mar_load,
    output reg pc_load,
    output reg pc_inc,
    output reg a_load,
    output reg b_load,
    output reg [2:0] alu_sel,
    output reg ccr_load,
    output reg irq_load,
    output reg reset_irq_pending,
    output reg [1:0] bus2_sel,
    output reg [1:0] bus1_sel,
    output reg backup_load,
    output reg backup_restore,
    output reg write,
    input wire [7:0] ir,
    input wire irq_pending,
    input wire [3:0] ccr_result,
    input wire reset,
    input wire clock
    );
    
    reg [7:0] current_state;
    reg [7:0] next_state;
    
    parameter S_FETCH_0 = 0; // MOVE PC TO MAR 
    parameter S_FETCH_1 = 1; // PC + 1
    parameter S_FETCH_2 = 2; // FROM_MEMORY TO IR
     
    parameter S_DECODE_3 = 3;
    
    parameter S_LDA_IMM_4 = 4; // MOVE PC TO MAR
    parameter S_LDA_IMM_5 = 5; // PC + 1
    parameter S_LDA_IMM_6 = 6; // FROM_MEMORY TO A
    
    parameter S_LDA_DIR_4 = 7; // MOVE PC TO MAR
    parameter S_LDA_DIR_5 = 8; // PC + 1
    parameter S_LDA_DIR_6 = 9; // FROM_MEMORY TO MAR
    parameter S_LDA_DIR_7 = 10; // 
    parameter S_LDA_DIR_8 = 11; // FROM_MEMORY TO A
    
    parameter S_STA_DIR_4 = 12; // MOVE PC TO MAR
    parameter S_STA_DIR_5 = 13; // PC + 1
    parameter S_STA_DIR_6 = 14; // A TO TO_MEMORY
    parameter S_STA_DIR_7 = 15; // WRITE 1
    
    parameter S_LDB_IMM_4 = 16; // MOVE PC TO MAR
    parameter S_LDB_IMM_5 = 17; // PC + 1
    parameter S_LDB_IMM_6 = 18; // FROM_MEMORY TO B
    
    parameter S_LDB_DIR_4 = 19; // MOVE PC TO MAR
    parameter S_LDB_DIR_5 = 20; // PC + 1
    parameter S_LDB_DIR_6 = 21; // FROM_MEMORY TO MAR
    parameter S_LDB_DIR_7 = 22; // 
    parameter S_LDB_DIR_8 = 23; // FROM_MEMORY TO B
    
    parameter S_STB_DIR_4 = 24; // MOVE PC TO MAR
    parameter S_STB_DIR_5 = 25; // PC + 1
    parameter S_STB_DIR_6 = 26; // B TO TO_MEMORY
    parameter S_STB_DIR_7 = 27; // WRITE 1
    
    parameter S_BRA_4 = 28; // MOVE PC TO MAR
    parameter S_BRA_5 = 29; // PC + 1
    parameter S_BRA_6 = 30; // FROM_MEMORY TO PC
    
    parameter S_BEQ_4 = 31; // MOVE PC TO MAR
    parameter S_BEQ_5 = 32; // CCR_LOAD
    parameter S_BEQ_6 = 33; // FROM_MEMORY TO PC
    parameter S_BEQ_7 = 34; // PC + 1
    
    //INIT, ENTER AND EXIT IRQ
    parameter S_EIRQ_0 = 43; // BACKUP PC, A AND B 
    parameter S_EIRQ_1 = 44; // MOVE IR TO PC 
    parameter S_EIRQ_2 = 45; // RESET PENDING IRQ
    parameter S_EIRQ_3 = 46;
    
    parameter S_LDIRQ_4 = 47; // MOVE PC TO MAR
    parameter S_LDIRQ_5 = 48; // PC + 1
    parameter S_LDIRQ_6 = 49; // FROM_MEMORY TO IRQ
    
    parameter S_EIRQ_4 = 50; // RESTORE REGISTER PC, A AND B

    parameter S_ADD_AB_4 = 35; // A to BUS1
    parameter S_SUB_AB_4 = 36; // A to BUS1
    parameter S_AND_AB_4 = 37; // A to BUS1
    parameter S_OR_AB_4 = 38; // A to BUS1
    parameter S_INC_A_4 = 39; // A to BUS1
    parameter S_INC_B_4 = 40; // A to BUS1
    parameter S_DEC_A_4 = 41; // A to BUS1
    parameter S_DEC_B_4 = 42; // A to BUS1
    
    always @ (posedge clock or negedge reset)
    begin: STATE_MEMORY
        if(!reset)
            current_state = S_FETCH_0;
        
        else
            current_state = next_state;
    end
    
    always @ (current_state, ir, ccr_result, irq_pending)
    begin: NEXT_STATE_LOGIC
        case(current_state)
            S_FETCH_0:
                if (!irq_pending)
                    next_state = S_FETCH_1;
                        
                else
                    next_state = S_EIRQ_0;
            
            S_FETCH_1:
                next_state = S_FETCH_2;
                
            S_FETCH_2:
                next_state = S_DECODE_3;
                
            S_DECODE_3:
            
                //LOADS AND STORES
                if(ir == 8'h86)
                    next_state = S_LDA_IMM_4;
                    
                else if (ir == 8'h87)
                    next_state = S_LDA_DIR_4;
                    
                else if (ir == 8'h96)
                    next_state = S_STA_DIR_4;
                
                else if (ir == 8'h88)
                    next_state = S_LDB_IMM_4;
                
                else if (ir == 8'h89)
                    next_state = S_LDB_DIR_4;
                
                else if (ir == 8'h97)
                    next_state = S_STB_DIR_4;
                    
                else if (ir == 8'h98)
                    next_state = S_LDIRQ_4;
                
                else if (ir == 8'h99)
                    next_state = S_EIRQ_4;
                    
                //DATA MANIPULATIONS
                else if (ir == 8'h42)
                    next_state = S_ADD_AB_4;
                    
                else if (ir == 8'h43)
                    next_state = S_SUB_AB_4;
                
                else if (ir == 8'h44)
                    next_state = S_AND_AB_4;
                
                else if (ir == 8'h45)       
                    next_state = S_OR_AB_4;
                
                else if (ir == 8'h46)       
                    next_state = S_INC_A_4;
                
                else if (ir == 8'h47)       
                    next_state = S_INC_B_4;
                    
                else if (ir == 8'h48)       
                    next_state = S_DEC_A_4;
                    
                else if (ir == 8'h49)       
                    next_state = S_DEC_B_4;
                    
                //BRANCHES
                else if (ir == 8'h20)
                    next_state = S_BRA_4; 
                
                else if (ir == 8'h23 && ccr_result[2] )
                    next_state = S_BEQ_4;
                
                else if (ir == 8'h23 && !ccr_result[2] )
                    next_state = S_BEQ_7;
            
            S_LDA_IMM_4:
                next_state = S_LDA_IMM_5;
            
            S_LDA_IMM_5:
                next_state = S_LDA_IMM_6;
                
            S_LDA_IMM_6:
                next_state = S_FETCH_0;
                
            S_LDA_DIR_4:
                next_state = S_LDA_DIR_5;
            
            S_LDA_DIR_5:
                next_state = S_LDA_DIR_6;
            
            S_LDA_DIR_6:
                next_state = S_LDA_DIR_7;
                
            S_LDA_DIR_7:
                next_state = S_LDA_DIR_8;
                
            S_LDA_DIR_8:
                next_state = S_FETCH_0;
                
            S_STA_DIR_4:
                next_state = S_STA_DIR_5;
            
            S_STA_DIR_5:
                next_state = S_STA_DIR_6;
            
            S_STA_DIR_6:
                next_state = S_STA_DIR_7;
                
            S_STA_DIR_7:
                next_state = S_FETCH_0;
                
            S_LDB_IMM_4:
                next_state = S_LDB_IMM_5;
            
            S_LDB_IMM_5:
                next_state = S_LDB_IMM_6;
                
            S_LDB_IMM_6:
                next_state = S_FETCH_0;
                
            S_LDB_DIR_4:
                next_state = S_LDB_DIR_5;
            
            S_LDB_DIR_5:
                next_state = S_LDB_DIR_6;
            
            S_LDB_DIR_6:
                next_state = S_LDB_DIR_7;
                
            S_LDB_DIR_7:
                next_state = S_LDB_DIR_8;
                
            S_LDB_DIR_8:
                next_state = S_FETCH_0;
                
            S_STB_DIR_4:
                next_state = S_STB_DIR_5;
            
            S_STB_DIR_5:
                next_state = S_STB_DIR_6;
            
            S_STB_DIR_6:
                next_state = S_STB_DIR_7;
                
            S_STB_DIR_7:
                next_state = S_FETCH_0;
            
            S_EIRQ_0:
                next_state = S_EIRQ_1;
                
            S_EIRQ_1:
                next_state = S_EIRQ_2;
                
            S_EIRQ_2:
                next_state = S_EIRQ_3;   
                
            S_EIRQ_3:
                next_state =  S_FETCH_0;
            
            S_LDIRQ_4:
                next_state = S_LDIRQ_5;
                
            S_LDIRQ_5:
                next_state = S_LDIRQ_6;
                
            S_LDIRQ_6:
                next_state = S_FETCH_0;
                
            S_EIRQ_4:
                next_state = S_FETCH_0;
                          
            S_BRA_4:
                next_state = S_BRA_5;
            
            S_BRA_5:
                next_state = S_BRA_6;
            
            S_BRA_6:
                next_state = S_FETCH_0;
                
            S_BEQ_4:
                next_state = S_BEQ_5;
            
            S_BEQ_5:
                next_state = S_BEQ_6;
                
            S_BEQ_6:
                next_state = S_BEQ_7;
            
            S_BEQ_7:
                next_state = S_FETCH_0;
                
            S_ADD_AB_4:
                next_state = S_FETCH_0;
            
            S_SUB_AB_4:
                next_state = S_FETCH_0;
                
            S_AND_AB_4:
                next_state = S_FETCH_0;
                
            S_OR_AB_4:
                next_state = S_FETCH_0;
                
            S_INC_A_4:
                next_state = S_FETCH_0;
                
            S_INC_B_4:
                next_state = S_FETCH_0;
                
            S_DEC_A_4:
                next_state = S_FETCH_0;
                
            S_DEC_B_4:
                next_state = S_FETCH_0;
        endcase
    end
   
    always @ (current_state)
    begin: OUTPUT_LOGIC
        case(current_state)
            S_FETCH_0:
                begin
                    if (!irq_pending)
                        begin
                            ir_load <= 0;
                            mar_load <= 1;
                            pc_load <= 0;
                            pc_inc <= 0;
                            a_load <= 0;
                            b_load <= 0;
                            alu_sel <= 0;
                            ccr_load <= 0;
                            bus1_sel <= 2'b00;
                            bus2_sel <= 2'b01;
                            write <= 0;
                            backup_load <= 0;
                            backup_restore <= 0;
                            irq_load <= 0;
                            reset_irq_pending <= 0;
                        end 
                 end
              
              S_FETCH_1:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end  
              
              S_FETCH_2:
                begin
                    ir_load <= 1;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end 
                 
             S_LDA_IMM_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_LDA_IMM_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDA_IMM_6:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDA_DIR_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_LDA_DIR_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
            
            S_LDA_DIR_6:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
            
            S_LDA_DIR_7:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDA_DIR_8:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_STA_DIR_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_STA_DIR_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_STA_DIR_6:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_STA_DIR_7:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 1;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_LDB_IMM_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_LDB_IMM_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDB_IMM_6:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 1;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDB_DIR_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_LDB_DIR_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
            
            S_LDB_DIR_6:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
            
            S_LDB_DIR_7:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_LDB_DIR_8:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 1;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
                
            S_STB_DIR_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_STB_DIR_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_STB_DIR_6:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_STB_DIR_7:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b10;
                    bus2_sel <= 2'b00;
                    write <= 1;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_EIRQ_0:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 1;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
              
              S_EIRQ_1:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 1;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b11;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end  
              
              S_EIRQ_2:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 1;
                 end
                 
              S_EIRQ_3:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                 end
            
            S_LDIRQ_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end    
            
            S_LDIRQ_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_LDIRQ_6:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 1;
                    reset_irq_pending <= 0;
                end    
            
            S_EIRQ_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 1;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end 
                
            S_BRA_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_BRA_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_BRA_6:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 1;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_BEQ_4:
                begin
                    ir_load <= 0;
                    mar_load <= 1;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b01;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_BEQ_5:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_BEQ_6:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 1;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b10;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
            
            S_BEQ_7:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 1;
                    a_load <= 0;
                    b_load <= 0;
                    alu_sel <= 0;
                    ccr_load <= 0;
                    bus1_sel <= 2'b00;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
            S_ADD_AB_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b000;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
             S_SUB_AB_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b001;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_AND_AB_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b010;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_OR_AB_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b011;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_INC_A_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b100;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_INC_B_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 1;
                    alu_sel <= 3'b101;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_DEC_A_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 1;
                    b_load <= 0;
                    alu_sel <= 3'b110;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
                
              S_DEC_B_4:
                begin
                    ir_load <= 0;
                    mar_load <= 0;
                    pc_load <= 0;
                    pc_inc <= 0;
                    a_load <= 0;
                    b_load <= 1;
                    alu_sel <= 3'b111;
                    ccr_load <= 1;
                    bus1_sel <= 2'b01;
                    bus2_sel <= 2'b00;
                    write <= 0;
                    backup_load <= 0;
                    backup_restore <= 0;
                    irq_load <= 0;
                    reset_irq_pending <= 0;
                end
        endcase
    end   
endmodule
