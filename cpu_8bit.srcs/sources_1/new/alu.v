`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 14:18:46
// Design Name: 
// Module Name: data_path
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu(
    output reg [7:0] result,
    output reg [3:0] nzvc,
    input wire [7:0] a,
    input wire [7:0] b,
    input wire [2:0] alu_sel
    );
    
    always @(alu_sel, a, b)
    begin: MUX
        case(alu_sel)
            3'b000:
                begin
                    {nzvc[0],result} = a + b;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
           
           3'b001:
                begin
                    {nzvc[0],result} = a - b;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
              
           3'b010:
                begin
                    {nzvc[0],result} = a & b;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
            
            3'b011:
                begin
                    {nzvc[0],result} = a | b;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end  
           
           3'b100:
                begin
                    {nzvc[0],result} = a + 1;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end 
           
           3'b101:
                begin
                    {nzvc[0],result} = b + 1;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
           
           3'b110:
                begin
                    {nzvc[0],result} = a - 1;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
           
           3'b111:
                begin
                    {nzvc[0],result} = b - 1;
                    
                    nzvc[3] = result[7];
                    
                    if(result == 0)
                        nzvc[2] = 1;
                    else
                        nzvc[2] = 0;
                        
                   if((a[7] == 0 && b[7] == 0 && result[7] == 1) ||
                      (a[7] == 1 && b[7] == 1 && result[7] == 0))
                      nzvc[1] = 1;
                      
                   else
                      nzvc[1] = 0;
              end
               
        endcase
    end
    
endmodule
