`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 12:26:28
// Design Name: 
// Module Name: memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module memory(
    output reg [7:0] data_out,
    output reg [7:0] port_out_00,
    output reg [7:0] port_out_01,
    output reg [7:0] port_out_02,
    output reg [7:0] port_out_03,
    output reg [7:0] port_out_04,
    output reg [7:0] port_out_05,
    output reg [7:0] irq_number,
    output reg irq_number_load,
    output reg irq_pending,
    input wire [7:0] address,
    input wire [7:0] data_in,
    input wire write,
    input wire reset_irq_pending,
    input wire [7:0] port_in_00,
    input wire [7:0] port_in_01,
    input wire [7:0] port_in_02,
    input wire [7:0] port_in_03,
    input wire [7:0] port_in_04,
    input wire [7:0] port_in_05,
    input wire reset,
    input wire clock
    );
    
    wire [7:0] rw_data_out;
    wire [7:0] rw_data_out_timer;
    wire timer_irq;
    reg load_timer;
    
    rw_200x8_async rw_mem(
            .data_out(rw_data_out),
            .address(address),
            .data_in(data_in),
            .write(write),
            .clock(clock)
            );
           
     //Peripheral devices 200-224
     fourbit_timer_irq timer(
            .irq(timer_irq),
            .data_out(rw_data_out_timer),
            .address(address),
            .data_in(data_in),
            .write(write),
            .reset(reset),
            .clock(clock)
            );
     
     always @(port_in_00, port_in_01, port_in_02, port_in_03, port_in_04, port_in_05)
     begin
        irq_pending <= 1;
        irq_number <= 1;
        irq_number_load <= 1; 
     end
     
     always @(timer_irq)
     begin
        irq_pending <= 1;
        irq_number <= 2;
        irq_number_load <= 1; 
     end
     
     initial
     begin
        irq_number <= 0;
        irq_number_load <= 0;
        irq_pending <= 0;
     end
     
     always @(posedge clock or negedge reset)     
     begin
        if(!reset)
            irq_pending <= 0;
            
         else if (reset_irq_pending)
            begin
                irq_pending <= 0;
                irq_number <= 0;
                irq_number_load <= 1;
            end
     end
            
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_00 = 0;
            
         else
         begin
            if(address == 224 && write)
                port_out_00 = data_in;
         end
     end
     
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_01 = 0;
            
         else
         begin
            if(address == 225 && write)
                port_out_01 = data_in;
         end
     end
     
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_02 = 0;
            
         else
         begin
            if(address == 226 && write)
                port_out_02 = data_in;
         end
     end
     
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_03 = 0;
            
         else
         begin
            if(address == 227 && write)
                port_out_03 = data_in;
         end
     end
     
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_04 = 0;
            
         else
         begin
            if(address == 228 && write)
                port_out_04 = data_in;
         end
     end
     
     always @(posedge clock or negedge reset)
     begin
        if(!reset)
            port_out_05 = 0;
            
         else
         begin
            if(address == 229 && write)
                port_out_05 = data_in;
         end
     end
     
    
    always @(address,rw_data_out,port_in_00,
             port_in_01,port_in_02,port_in_03,
             port_in_04,port_in_05)
             
        begin:MUX
        
            if(address >= 0 && address < 200)
                data_out = rw_data_out;
            
            if(address >= 200 && address < 224)
                data_out = rw_data_out_timer;
            
            else if (address == 230)
                data_out = port_in_00;
           
           else if (address == 231)
                data_out = port_in_01;
                
           else if (address == 232)
                data_out = port_in_02;
                
           else if (address == 233)
                data_out = port_in_03;
                
           else if (address == 234)
                data_out = port_in_04;
                
           else if (address == 235)
                data_out = port_in_05;
        end    
endmodule
