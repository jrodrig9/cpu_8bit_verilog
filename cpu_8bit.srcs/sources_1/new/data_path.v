`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02.08.2022 14:18:46
// Design Name: 
// Module Name: data_path
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module data_path(
    output reg [7:0] to_memory,
    output reg [7:0] address,
    output reg [7:0] ir,
    output reg [3:0] ccr,
    input wire [7:0] from_memory,
    input wire [2:0] alu_sel,
    input wire [1:0] bus2_sel,
    input wire [1:0] bus1_sel,
    input wire [7:0] irq_number_in,
    input wire ir_load,
    input wire mar_load,
    input wire pc_load,
    input wire pc_inc,
    input wire a_load,
    input wire b_load,
    input wire backup_load,
    input wire backup_restore,
    input wire ccr_load,
    input wire irq_load,
    input wire irq_number_load,
    input wire reset,
    input wire clock
    );
    
    wire [7:0] alu_res;
    wire [3:0] nzvc;
    reg [7:0] mar;
    reg [7:0] irq;
    reg [7:0] irq_number;
    reg [7:0] pc;
    reg [7:0] a;
    reg [7:0] b;
    reg [7:0] pc_backup;
    reg [7:0] a_backup;
    reg [7:0] b_backup;
    reg [7:0] bus1;
    reg [7:0] bus2;
    
    alu unit(
        alu_res,
        nzvc,
        bus1,
        b,
        alu_sel
    );
    
    initial
    begin
        pc <= 0;
        ir <= 0;
        mar <= 0;
        a <= 0;
        b <= 0;
    end
    
    always @ (bus1, mar)
    begin
        to_memory <= bus1;
        address <= mar;
    end
    
    always @ (bus1_sel, pc, a, b)
    begin: BUS1_MUX
        case(bus1_sel)
             2'b00:
                bus1 <= pc;
             
             2'b01:
                bus1 <= a;
             
             2'b10:
                bus1 <= b;
                
             2'b11:
                bus1 <= irq;
                
             default:
                bus1 <= 8'hXX;
        endcase
    end
    
    always @ (bus2_sel, alu_res, bus1, from_memory)
    begin: BUS2_MUX
        case(bus2_sel)
             2'b00:
                bus2 <= alu_res;
             
             2'b01:
                bus2 <= bus1;
             
             2'b10:
                bus2 <= from_memory;
                
             default:
                bus1 <= 8'hXX;
        endcase
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_IR
        if(!reset)
            ir <= 8'h00;
        
        else if(ir_load)
            ir <= bus2;
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_MAR
        if(!reset)
            mar <= 8'h00;
        
        else if(mar_load)
            mar <= bus2;
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_IRQ
        if(!reset)
            irq <= 8'h00;
        
        else if(irq_load)
            irq <= bus2;
            
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_PC
        if(!reset)
            pc <= 8'h00;
        
        else if(pc_load)
            pc <= bus2;
            
        else if(pc_inc)
            pc <= pc +1;
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_A
        if(!reset)
            a <= 8'h00;
        
        else if(a_load)
            a <= bus2;
    end
    
    always @ (posedge clock or negedge reset)
    begin: BUS2_REGISTER_B
        if(!reset)
            b <= 8'h00;
        
        else if(b_load)
            b <= bus2;
    end
    
    always @ (posedge clock or negedge reset)
    begin:CONDITION_CODE_REGISTER
        if(!reset)
            ccr <= 0;
        else if(ccr_load)
            ccr <= nzvc;
    end
    
    always @ (posedge clock or negedge reset)
    begin:IRQ_NUMBER_REGISTER
        if(!reset)
            irq_number <= 0;
        else if(irq_number_load)
            irq_number <= irq_number_in;
    end
    
    always @ (posedge clock or negedge reset)
    begin:BACKUP_REGISTER
        if(!reset)
            begin
                pc_backup <= 0;
                a_backup <= 0;
                b_backup <= 0;
            end
            
        else if(backup_load)
            begin
                pc_backup <= pc;
                a_backup <= a;
                b_backup <= b;
            end
            
        else if(backup_restore)
            begin
                pc <= pc_backup;
                a <= a_backup;
                b <= b_backup;
            end
    end
    
    
endmodule
